///////////////////////////////////////////////////////////////////////////////
/// \file test.cpp
///
/// \author Ivan Galvez Junquera
/// \date 27/12/2012
///
/// \brief Very simple test for Binary Tree.
///
/// Copyright 2012-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

#include <cstdlib>
#include <string>

#include "Btree.h"

int main()
{
	//Initialising a btree for char data
	Tree<char> btree;
	Node<char> * node = NULL;

	//Populate the tree
	node = btree.insert (node, 'A');
	node = btree.insert (node, 'B');
	node = btree.insert (node, 'C');
	node = btree.insert (node, 'D');

	//Pre-order printing
	std::cout << "Pre-order printing:" << std::endl;
	btree.preorder(node);
	std::cout << std::endl;

	//Post-order printing
	std::cout << "Post-order printing:" << std::endl;
	btree.postorder(node);
	std::cout << std::endl;

	//In-order printing
	std::cout << "In-order printing:" << std::endl;
	btree.inorder(node);
	std::cout << std::endl;
}
