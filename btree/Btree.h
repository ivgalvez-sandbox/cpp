//////////////////////////////////////////////////////////////////////////////
/// \file Tree.h
///
/// \author Ivan Galvez Junquera
/// \date 27/12/2012
///
/// \brief Simple binary tree data structure
///
/// Copyright 2012-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

#ifndef TREE_H
#define TREE_H

#include <iostream>

template <typename T>
struct Node
{
	//! \brief Data contained in the node
	T data;

	//! \brief Reference to left child
	Node<T> * left;

	//! \brief Reference to right child
	Node<T> * right;
};

template <typename T>
class Tree
{
	private:
		unsigned int m_reference;

	public:
		//! \brief Constructor
		Tree ()
		{
			m_reference = 1;
		};

		//! \brief Destructor.
		virtual ~Tree ()
			{};

		//! \brief Simple insertion operation
		Node<T> * insert (Node<T> * tree, T data)
		{
			//First node (root of the tree)
			if ( tree == NULL )
			{
				tree = new Node<T>;
				tree->left = tree->right = NULL;
				tree->data = data;
				m_reference++;
			}
			// Not the root, but any other node
			else if ( m_reference%2 == 0 )
			{
				// Left insertion
				tree->left = insert ( tree->left, data );
			}
			else
			{
				// Right insertion
				tree->right = insert ( tree->right, data );
			}

			return  tree;
		}

		//! \brief pre-order printing
		void preorder ( Node<T> * tree )
		{
			if ( tree != NULL )
			{
				std::cout << tree->data;
				preorder ( tree->left );
				preorder ( tree->right );
			}
		};

		//! \brief post-order printing
		void postorder ( Node<T> * tree )
		{
			if ( tree!=NULL )
			{
				postorder ( tree->left );
				postorder ( tree->right );
				std::cout << tree->data;
			}
		};

		//! \brief in-order printing
		void inorder ( Node<T> * tree )
		{
			if ( tree != NULL )
			{
				inorder ( tree->left );
				std::cout << tree->data;
				inorder ( tree->right );
			}
		};

};
#endif							 //TREE_H
