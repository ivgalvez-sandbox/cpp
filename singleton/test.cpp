///////////////////////////////////////////////////////////////////////////////
/// \file test.cpp
///
/// \author Ivan Galvez Junquera
/// \date 23/12/2008
///
/// \brief Simple test for Singleton instantiation.
/// C++11 version using veriadic templates.
///
/// Copyright 2008-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

// Use:
// g++ -std=c++11 -DHAS_VARIADIC_TEMPLATES -o test.exe test.cpp
// To build the example with variadic templates.
#ifdef HAS_VARIADIC_TEMPLATES
#	warning ("Building C++11 version using variadic templates")
#	include "Singleton_variadic.h"
#else
#	include "Singleton.h"
#endif


static signed int counterA = 0;
class A
{
	public:
		A (std::string const & name, std::string const & foo)
			: m_name(name)
		{
			counterA++;
			std::cout << "Created object A with name = " << m_name << std::endl;
			std::cout << "Reference counter for A = " << counterA << std::endl;
		};

		~A()
		{
			counterA--;
			std::cout << "Destroyed object A with name = " << m_name << std::endl;
			std::cout << "Reference counter for A = " << counterA << std::endl;
		};
	
	public:
		std::string m_name;
};

struct B
{
	void do_work(){std::cout << "B - do_work" << std::endl;};
};

int main()
{

	std::cout << "------------------------------------------------" << std::endl;
	std::cout << "--- Using class A, which is not a Singleton --- " << std::endl;
	std::cout << "------------------------------------------------" << std::endl;
	{
		std::cout << "Instantiate A a1(\"a_name\")" << std::endl;
		A a1("a_name", "foo");
		std::cout << std::endl;
		std::cout << "Create object A * a2 = new A(\"a_name\");" << std::endl;
		A * a2 = new A("a_name", "foo");
		std::cout << std::endl;
		std::cout << "Copy a1 in a3" << std::endl;
		A a3(a1);
		std::cout << std::endl;
		std::cout << "Assign a1 to a4" << std::endl;
		A a4 = a1;
		std::cout << "Now explicitely delete a2;" << std::endl;
		delete a2;
		std::cout << std::endl;
	}
	std::cout << "Reference counter is -2 because we destroyed two copies!!" << std::endl;
	std::cout << "------------------------------------------------" << std::endl;
	std::cout << std::endl;

	//Reset reference counter
	counterA = 0;

	std::cout << "------------------------------------------------" << std::endl;
	std::cout << "--- Using class A as a Singleton --- " << std::endl;
	std::cout << "------------------------------------------------" << std::endl;
	{
		typedef common::Singleton<A> SingleA;
		std::cout << "Accessed to " << SingleA::getInstance("A - instance 1", "foo").m_name << std::endl;
		std::cout << std::endl;
		std::cout << "Accessed to " << SingleA::getInstance("A - instance 1", "foo").m_name << std::endl;
		std::cout << std::endl;
		std::cout << "Accessed to " << SingleA::getInstance("A - trying to create instance 2", "boo").m_name << std::endl;
		std::cout << std::endl;

		//Constructor is protected!!
		//SingleA a1;
	}
	std::cout << "------------------------------------------------" << std::endl;
	std::cout << std::endl;

	typedef common::Singleton<B> SingleB;
	SingleB::getInstance().do_work();
	
	return 0;
}
