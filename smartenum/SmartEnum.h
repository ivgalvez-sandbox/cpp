///////////////////////////////////////////////////////////////////////////////
/// \file SmartEnum.h
///
/// \author Ivan Galvez Junquera
/// \date 23/03/2010
/// \ingroup common
///
/// \brief Templatized Smart Enum.
///
/// Copyright 2010-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

//! \addtogroup common Common: Classes and functions of common use.
//! @{

#ifndef SMART_ENUM_H
#define SMART_ENUM_H

//System headers
#include <string>
#include <iostream>

namespace common
{
	//! \brief The SmartEnum idiom allows the use of an enumerator either
	//! as an integer value or a string with the name of the value.
	//!
	//! \ingroup common
	//!
	//! \author Ivan Galvez Junquera
	template <typename T>
		class SmartEnum
	{
			//! \brief Operator insertion on a stream.
			//!
			//! This method calls SmartEnum::display() with insertor syntax.
			//!
			//! \ingroup common
			//!
			//! \param ostr The destination output stream.
			//! \param val Instance to display.
			//!
			//! \sa SmartEnum::display()
			friend std::ostream &operator<<(std::ostream &ostr, SmartEnum<T> const &val)
			{
				val.display(ostr);
				return ostr;
			};

		public:
			//! \brief Default constructor.
			//!
			//! The enumerator will be initialized to the first defined element.
			SmartEnum()
			{
				m_enum = (T)m_map[0].value;
			};

			//! \brief Constructor receiving an int value.
			//!
			//! \param val Initialization value for the enumerator.
			SmartEnum(int val)
			{
				m_enum = asEnum(val);
			};

			//! \brief Constructor receiving a string value.
			//!
			//! \param name Initialization value for the enumerator.
			SmartEnum(const std::string & name)
			{
				m_enum = asEnum(name);
			};

			//! \brief Constructor receiving an enumerator value.
			//!
			//! \param enumerator Initialization value for the enumerator.
			SmartEnum( T enumerator )
			{
				m_enum = enumerator;
			};

			//! \brief Destructor.
			virtual ~SmartEnum()
			{};

			void display(std::ostream &ostr) const
			{
				ostr << asName(m_enum);
			};


			//! \brief Assign operator to int.
			//!
			//! \param val Number of the enumerator to be assigned to.
			//!
			//! \returns the new enumerator.
			T operator=(int val)
			{
				m_enum = asEnum(val);
				return m_enum;
			};

			//! \brief Assign operator to smart enum.
			//!
			//! \param e Enumerator to be assigned to.
			//!
			//! \returns the new enumerator.
			T operator=(T e)
			{
				m_enum = e;
				return m_enum;
			};

			//! \brief Assign operator to std::string.
			//!
			//! \param sz String with the name of the enumerator to be assigned to.
			//!
			//! \returns the new enumerator.
			T operator=(const std::string & sz)
			{
				m_enum = asEnum(sz);
				return m_enum;
			};

			//! \brief Overloaded equal to operator
			//!
			//! \param sz String with the name of the enumerator to be compared to.
			//!
			//! \returns true if both enumerators are equal.
			bool operator==(const std::string & sz)
			{
				return m_enum == asEnum(sz);
			};

			//! \brief Overloaded not equal to operator
			//!
			//! \param sz String with the name of the enumerator to be compared to.
			//!
			//! \returns true if both enumerators are not equal.
			bool operator!=(const std::string & sz)
			{
				return m_enum != asEnum(sz);
			};

			//! \brief Overloaded cast operator to int or enum.
			//!
			//! \returns The value of the enumerator as int or enum.
			operator T () const
			{
				return m_enum;
			};

			//! \brief Overloaded cast operator to const std::string &.
			//!
			//! \returns The description of the enumerator value as const std::string &.
			operator const std::string & () const
			{
				return asName(m_enum);
			};

			//! \brief Overloaded cast operator to const char *.
			//!
			//! \returns The description of the enumerator value as const char *.
			operator const char * () const
			{
				return (asName(m_enum)).c_str();
			};

		private:
			static T asEnum(const std::string & name)
			{
				for (int i = 0; i < m_map_size; ++i)
				{
					if(name == m_map[i].description)
					{
						return (T)m_map[i].value;
					}
				}
				return (T)m_map[0].value;
			};

			static T asEnum(int val)
			{
				for (int i = 0; i < m_map_size; ++i)
				{
					if(m_map[i].value == val)
					{
						return (T)m_map[i].value;
					}
				}
				return (T)m_map[0].value;
			};

			static const std::string & asName(int val)
			{
				for (int i = 0; i < m_map_size; ++i)
				{
					if(m_map[i].value == val)
					{
						return m_map[i].description;
					}
				}
				return m_map[0].description;
			};

		private:
			T m_enum;

			struct Map_type
			{
				int value;
				std::string description;
			};
			static Map_type m_map[];

			static const int m_map_size;
		};
}
#endif // SMART_ENUM_H
//! @}
