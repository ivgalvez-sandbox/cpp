///////////////////////////////////////////////////////////////////////////////
/// \file test.cpp
///
/// \author Ivan Galvez Junquera
/// \date 23/03/2010
///
/// \brief Simple test for Templatized Smart Enum.
///
/// Copyright 2010-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>
#include <sstream>
#include <string>
#include <cstdio>

#include "SmartEnum.h"

//This code would be located in your MyEnum.cpp file
enum MyEnum
{
	NUMBER_0 = 0,
	NUMBER_1 = 1,
	NUMBER_2 = 2,
	NUMBER_3 = 3,
	NUM_SIZE //Can be automatically guessed
};
template<> const int common::SmartEnum<MyEnum>::m_map_size = NUM_SIZE;
template<> common::SmartEnum<MyEnum>::Map_type common::SmartEnum<MyEnum>::m_map[] =
{
	{NUMBER_0, "NUMBER_0"},
	{NUMBER_1, "NUMBER_1"},
	{NUMBER_2, "NUMBER_2"},
	{NUMBER_3, "NUMBER_3"},
	{NUM_SIZE, "NUM_SIZE"}
};

//Optional:
typedef common::SmartEnum<MyEnum> MyEnum_type; //Typedef for ease use of enumerator
//End of MyEnum.cpp

//This code would be located in your MyTest.cpp file
enum MyTest
{
	TEST_31 = 31,
	TEST_32 = 32,
	TEST_33 = 33,
	TEST_SIZE = 3 //Must be defined
};
template<> const int common::SmartEnum<MyTest>::m_map_size = TEST_SIZE;
template<> common::SmartEnum<MyTest>::Map_type common::SmartEnum<MyTest>::m_map[] =
{
	{TEST_31, "TEST_31"},
	{TEST_32, "TEST_32"},
	{TEST_33, "TEST_33"},
	{TEST_SIZE, "TEST_SIZE"}
};
//End of MyTest.cpp

void printSeparator()
{
	std::cout << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;
};

int main()
{
	printSeparator();
	{
		std::cout << ">>> Creating a new SmartEnum with default type (NUMBER_0)" << std::endl;
		MyEnum_type enumerator;	 //Using the optional Typedef
		std::cout << "    Insertion operator      = " << enumerator << std::endl;
		printf (     "    Casting to int          = %d\n", (int)enumerator);
		printf (     "    Casting to const char * = %s\n", (const char *)enumerator);
		std::string var = enumerator;
		printf (     "    Casting to std::string  = %s\n", var.c_str());
		//Other possibility: printf (     "    Casting to std::string  = %s\n", ((std::string &)enumerator).c_str());
	}
	{
		std::cout << ">>> Creating a new SmartEnum with enum constructor (NUMBER_2)" << std::endl;
		common::SmartEnum<MyEnum> enumerator (NUMBER_2);
		std::cout << "    Insertion operator      = " << enumerator << std::endl;
		printf (     "    Casting to int          = %d\n", (int)enumerator);
		printf (     "    Casting to const char * = %s\n", (const char *)enumerator);
		std::string var = enumerator;
		printf (     "    Casting to std::string  = %s\n", var.c_str());
	}
	{
		std::cout << ">>> Creating a new SmartEnum with int constructor (1)" << std::endl;
		common::SmartEnum<MyEnum> enumerator(1);
		std::cout << "    Insertion operator      = " << enumerator << std::endl;
		printf (     "    Casting to int          = %d\n", (int)enumerator);
		printf (     "    Casting to const char * = %s\n", (const char *)enumerator);
		std::string var = enumerator;
		printf (     "    Casting to std::string  = %s\n", var.c_str());
	}
	{
		std::cout << ">>> Creating a new SmartEnum with std::string constructor ('NUMBER_2')" << std::endl;
		common::SmartEnum<MyEnum> enumerator("NUMBER_2");
		std::cout << "    Insertion operator      = " << enumerator << std::endl;
		printf (     "    Casting to int          = %d\n", (int)enumerator);
		printf (     "    Casting to const char * = %s\n", (const char *)enumerator);
		std::string var = enumerator;
		printf (     "    Casting to std::string  = %s\n", var.c_str());
	}

	printSeparator();
	{
		std::cout << ">>> Creating a new SmartEnum with default type (TEST_31)" << std::endl;
		common::SmartEnum<MyTest> enumerator;
		std::cout << "    Insertion operator      = " << enumerator << std::endl;
		printf (     "    Casting to int          = %d\n", (int)enumerator);
		printf (     "    Casting to const char * = %s\n", (const char *)enumerator);
		std::string var = enumerator;
		printf (     "    Casting to std::string  = %s\n", var.c_str());
	}
	{
		std::cout << ">>> Creating a new SmartEnum with default type (TEST_33)" << std::endl;
		common::SmartEnum<MyTest> enumerator (TEST_33);
		std::cout << "    Insertion operator      = " << enumerator << std::endl;
		printf (     "    Casting to int          = %d\n", (int)enumerator);
		printf (     "    Casting to const char * = %s\n", (const char *)enumerator);
		std::string var = enumerator;
		printf (     "    Casting to std::string  = %s\n", var.c_str());
	}
	{
		std::cout << ">>> Creating a new SmartEnum with int constructor (32)" << std::endl;
		common::SmartEnum<MyTest> enumerator(32);
		std::cout << "    Insertion operator      = " << enumerator << std::endl;
		printf (     "    Casting to int          = %d\n", (int)enumerator);
		printf (     "    Casting to const char * = %s\n", (const char *)enumerator);
		std::string var = enumerator;
		printf (     "    Casting to std::string  = %s\n", var.c_str());
	}
	{
		std::cout << ">>> Creating a new SmartEnum with int constructor ('TEST_33')" << std::endl;
		common::SmartEnum<MyTest> enumerator("TEST_33");
		std::cout << "    Insertion operator      = " << enumerator << std::endl;
		printf (     "    Casting to int          = %d\n", (int)enumerator);
		printf (     "    Casting to const char * = %s\n", (const char *)enumerator);
		std::string var = enumerator;
		printf (     "    Casting to std::string  = %s\n", var.c_str());
	}

	printSeparator();

}
