//////////////////////////////////////////////////////////////////////////////
/// \file Data.h
///
/// \author Ivan Galvez Junquera
/// \date 16/10/2014
///
/// \brief Data map for A* algorithm
///
/// Copyright 2014-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

#ifndef DATA_H
#define DATA_H

#include <map>

#include "Position.h"

//! \brief Available directions for a node (in a 2-dimensional map, either 4 or 8).
const unsigned int available_directions = 4;

// Horizontal size of the map
const unsigned int max_x = 10;
// Vertical size of the map
const unsigned int max_y = 10;
// Matrix representation of directions (0: East, 1: North, 2: West, 3: South)
const int directions_x[available_directions] = {1, 0, -1, 0};
const int directions_y[available_directions] = {0, 1, 0, -1};

struct DataCompare
{
	bool operator()(Position const & a, Position const & b) const 
	{
		return a < b;
	}
};

class Data
{
	public:
		//! \brief Default constructor.
		Data();

		//! \brief Destructor
		virtual ~Data();

		//! \brief Insert a new Position element in the data map.
		//!
		//! \param pos A position to store in the map.
		//! \param empty Indicate if this location is empty (walkable).
		void insert(Position const & pos, bool empty);

		//! \brief Fills the map with non-walkable max_x * max_y Positions.
		void fill();

		//! \brief Array subscript operator.
		//!
		//! \param pos Position to be examined.
		//! \returns true if the position is empty (walkable), false otherwise.
		const bool & operator [](Position const & pos) const;
	
	private:
		std::map<Position, bool, DataCompare> m_map;
};
#endif //DATA_H
