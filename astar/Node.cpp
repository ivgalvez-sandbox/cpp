//////////////////////////////////////////////////////////////////////////////
/// \file Node.cpp
///
/// \author Ivan Galvez Junquera
/// \date 07/10/2014
///
/// \brief Simple Node data structure
///
/// Copyright 2014-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

#include <cstdlib> //Provides std::abs

#include "Node.h"
#include "Data.h"


Node::Node (const Position & pos, unsigned int travelled, unsigned int estimated)
	: m_current_position(pos)
{ }

Node::Node (int x, int y, unsigned int travelled, unsigned int estimated)
	: m_current_position(x, y)
{ }

Node::~Node ()
{ }

Position const & Node::getPosition(void) const
{
	return m_current_position;
}

void Node::evaluateDistance(Position const & destination)
{
	m_fval = m_gval + heuristics(destination) * 10;
}

void Node::updateCovered(int direction)
{
	m_gval += (available_directions == 8 ? (direction % 2 == 0 ? 10 : 14) : 10);
}

unsigned int Node::getFval() const
{
	return m_fval;
}

unsigned int Node::getGval() const
{
	return m_gval;
}

void Node::display(std::ostream & ostr) const
{
	ostr << m_current_position;
}

int Node::heuristics (Position const & dest) const
{
	return (std::abs((int)(dest.getX() - m_current_position.getX())) + std::abs((int)(dest.getY() - m_current_position.getY())));
}

std::ostream & operator<<(std::ostream & ostr, Node const & val)
{
	val.display(ostr);
	return ostr;
}

bool operator<(Node const & a, Node const & b)
{
	return a.getFval() > b.getFval();
}
