///////////////////////////////////////////////////////////////////////////////
/// \file test.cpp
///
/// \author Ivan Galvez Junquera
/// \date 07/10/2014
///
/// \brief Very simple test for A * algorithm.
///
/// Copyright 2014-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <vector>
#include <list>

#include "Astar.h"
#include "Data.h"

int main()
{
	// Define cities
	Position Madrid = Position(2,4);
	Position Barcelona = Position(9,0);
	Position Albacete = Position(7,9);

	// Define roads (using C++11 Brace-Initialization)
	std::list<Position> road_a =
	{
		Position(7,8),
		Position(6,8),
		Position(5,8),
		Position(5,7),
		Position(4,7),
		Position(3,7),
		Position(3,6),
		Position(2,6),
		Position(2,5),
	};
	std::list<Position> road_b =
	{
		Position(3,4),
		Position(4,4),
		Position(5,4),
		Position(6,4),
		Position(7,4),
		Position(7,3),
		Position(7,2),
		Position(7,1),
		Position(7,0),
		Position(8,0),
	};

	// Build the area map
	Data spain = Data();
	spain.insert(Madrid, true);
	spain.insert(Barcelona, true);
	spain.insert(Albacete, true);
	for (std::list<Position>::const_iterator it = road_a.begin(); it != road_a.end(); ++it)
	{
		spain.insert(*it, true);
	}
	for (std::list<Position>::const_iterator it = road_b.begin(); it != road_b.end(); ++it)
	{
		spain.insert(*it, true);
	}
	spain.fill();


	// Calculate path from start to end
	std::list<Position> path = Astar(Albacete, Barcelona, spain);

	// Print result
	for (std::list<Position>::const_iterator it = path.begin(); it != path.end(); it++)
	{
		std::cout << *it << std::endl;;
		if ( (*it) == Albacete )
		{
			std::cout << ">> Start in Albacete" << std::endl;
		}
		else if ( (*it) == Madrid )
		{
			std::cout << ">> Passing over Madrid!" << std::endl;
		}
		else if ( (*it) == Barcelona )
		{
			std::cout << ">> Arrived to Barcelona!!" << std::endl;
		}
	}
	std::cout << "Total distance: " << path.size() << " units" << std::endl;
}
