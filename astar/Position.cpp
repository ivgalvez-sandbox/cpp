//////////////////////////////////////////////////////////////////////////////
/// \file Position.cpp
///
/// \author Ivan Galvez Junquera
/// \date 07/10/2014
///
/// \brief Simple Position data structure
///
/// Copyright 2014-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

#include "Position.h"

std::ostream & operator<<(std::ostream & ostr, Position const & val)
{
	val.display(ostr);
	return ostr;
}

Position::Position()
	: m_position_x(0)
	, m_position_y(0)
{ }

Position::Position (unsigned int pos_x, unsigned int pos_y)
	: m_position_x(pos_x)
	, m_position_y(pos_y)
{ }

Position::~Position()
{ }

unsigned int Position::getX(void) const
{
	return m_position_x;
}

unsigned int Position::getY(void) const
{
	return m_position_y;
}

void Position::set(unsigned int x, unsigned int y)
{
	m_position_x = x;
	m_position_y = y;
}

bool operator<(Position const & a, Position const & b)
{
	bool result = false;
	if (a.getX() < b.getX())
	{
		result = true;
	}
	else if (a.getX() == b.getX())
	{
		if (a.getY() < b.getY())
		{
			result = true;
		}
	}
	return result;
}

bool operator==(Position const & a, Position const & b)
{
	return ( (a.getX() == b.getX()) && (a.getY() == b.getY()) );
}

bool operator!=(Position const & a, Position const & b)
{
	return !( (a.getX() == b.getX()) && (a.getY() == b.getY()) );
}

void Position::display(std::ostream & ostr) const
{
	ostr << "(" << m_position_x << ", " << m_position_y << ")";
}
