//////////////////////////////////////////////////////////////////////////////
/// \file Node.h
///
/// \author Ivan Galvez Junquera
/// \date 07/10/2014
///
/// \brief Simple Node data structure
///
/// Copyright 2014-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

#ifndef NODE_H
#define NODE_H

#include <iostream>

#include "Position.h"

class Node
{
	//! \brief Operator insertion on a stream.
	//!
	//! \param ostr The destination output stream.
	//! \param val Instance to display.
	friend std::ostream & operator<<(std::ostream & ostr, Node const & val);

	//! \brief Operator for comparison, minor than.
	//!
	//! \param a A Node object to be compared.
	//! \param b A Node object to compared with.
	friend bool operator<(Node const & a, Node const & b);

	public:
		//! \brief Constructor.
		//!
		//! \param pos An instance of a Position class with the current position of the node.
		//! \param travelled The already covered distance (G).
		//! \param estimated Estimated distance to destination (F).
		Node ( Position const & pos, unsigned int travelled, unsigned int estimated );

		//! \brief Alternate constructor receiving coordinates.
		//!
		//! \param x Coordinate X of node's current position.
		//! \param y Coordinate Y of node's current position.
		//! \param travelled The already covered distance (G).
		//! \param estimated Estimated distance to destination (F).
		Node ( int x, int y, unsigned int travelled, unsigned int estimated );

		//! \brief Destructor.
		virtual ~Node ();

		//! \brief Read the position of the node.
		//!
		//! \returns An instance of Position.
		Position const & getPosition(void) const;

		//! \brief Evaluate distance from this node to a given destination.
		//!
 		//! This method calculates the estimated total cost from start to goal.
		//!
		//! \param destination Position of the desired destination.
		void evaluateDistance(Position const & destination);

		//! \brief Update the value of the distance already covered.
		//!
		//! \param direction An integer that represents a direction.
		void updateCovered(int direction);

		//! \brief Obtain the distance covered to the node.
		//!
		//! \returns An integer with the distance covered to the node.
		unsigned int getFval() const;

		//! \brief Obtain the estimated distance to destination.
		//!
		//! \returns An integer with the estimated distance to destination.
		unsigned int getGval() const;

		//! \brief Display the object in an output sstream.
		//!
		//! \param ostr The destination output stream.
		void display(std::ostream & ostr) const;

	private:
		//! \brief Heuristic estimation of pending distance.
		//!
		//! For simplicity we use a Manhattan distance calculation.
		//!
		//! \param dest Position of the desired destination.
		//! \returns An integer with the estimated distance.
		int heuristics (Position const & dest) const;

	private:
		//! \brief Position of the Node.
		Position m_current_position;

		//! \brief Distance covered to the node.
		unsigned int m_gval;

		//! \brief Estimated distance to destination.
		unsigned int m_fval;
};
#endif //NODE_H
