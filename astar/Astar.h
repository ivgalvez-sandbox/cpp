//////////////////////////////////////////////////////////////////////////////
/// \file Astar.h
///
/// \author Ivan Galvez Junquera
/// \date 16/10/2014
///
/// \brief A * Algorithm implementation
///
/// Copyright 2014-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

#ifndef ASTAR_H
#define ASTAR_H

#include <list>

#include "Position.h"
#include "Data.h"

//! \brief A * algorithm resolution
//!
//! \param start Contains the initial position.
//! \param end Destination point.
//!
//! \returns A list of positions that represents the route from start to end.
std::list<Position> & Astar(Position const & start, Position const & end, Data const & data);

#endif //ASTAR_H
