//////////////////////////////////////////////////////////////////////////////
/// \file Position.h
///
/// \author Ivan Galvez Junquera
/// \date 07/10/2014
///
/// \brief Simple 2-dimensional Position data structure
///
/// Copyright 2014-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

#ifndef POSITION_H
#define POSITION_H

#include <iostream>

struct Position
{
	//! \brief Operator insertion on a stream.
	//!
	//! \param ostr The destination output stream.
	//! \param val Instance to display.
	friend std::ostream & operator<<(std::ostream & ostr, Position const & val);

	//! \brief Operator for comparison, minor than.
	//!
	//! \param a A Position object to be compared.
	//! \param b A Position object to compared with.
	friend bool operator<(Position const & a, Position const & b);

	//! \brief Operator for comparison, equal to.
	//!
	//! \param a A Position object to be compared.
	//! \param b A Position object to compared with.
	friend bool operator==(Position const & a, Position const & b);

	//! \brief Operator for comparison, not equal to.
	//!
	//! \param a A Position object to be compared.
	//! \param b A Position object to compared with.
	friend bool operator!=(Position const & a, Position const & b);

	public:
		//! \brief Default constructor.
		Position();

		//! \brief Constructor.
		//!
		//! \param pos_x Coordinate X.
		//! \param pos_y Coordinate Y.
		Position(unsigned int pos_x, unsigned int pos_y);

		//! \brief Destructor
		virtual ~Position();

		//! \brief Get coordinate X.
		//!
		//! \returns An integer with coordinate X.
		unsigned int getX(void) const;

		//! \brief Get coordinate Y.
		//!
		//! \returns An integer with coordinate Y.
		unsigned int getY(void) const;

		//! \brief Set new coordinates.
		//!
		//! \param x New coordinate X.
		//! \param y New coordinate Y.
		void set(unsigned int x, unsigned int y);

		//! \brief Display the object in an output stream.
		//!
		//! \param ostr The destination output stream.
		void display(std::ostream & ostr) const;

	private:
		//! \brief Coordinate X
		unsigned int m_position_x;
	
		//! \brief Coordinate Y
		unsigned int m_position_y;
};
#endif //POSITION_H
