//////////////////////////////////////////////////////////////////////////////
/// \file Data.cpp
///
/// \author Ivan Galvez Junquera
/// \date 16/10/2014
///
/// \brief Data map for A* algorithm
///
/// Copyright 2014-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

#include "Data.h"

Data::Data()
{ }

Data::~Data()
{
	m_map.clear();
}

void Data::insert(Position const & pos, bool empty)
{
	m_map.insert(std::pair<Position const &, bool>(pos, empty));
}

void Data::fill()
{
	for (unsigned int x = 0; x < max_x; ++x)
	{
		for (unsigned int y = 0; y < max_y; ++y)
		{
			Position pos = Position(x,y);
			insert(pos, false);
		}
	}
}

const bool & Data::operator[](Position const & pos) const
{
	return m_map.find(pos)->second;
}
