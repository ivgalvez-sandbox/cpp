//////////////////////////////////////////////////////////////////////////////
/// \file Astar.cpp
///
/// \author Ivan Galvez Junquera
/// \date 16/10/2014
///
/// \brief A * Algorithm implementation
///
/// Copyright 2014-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

#include <queue>

#include "Astar.h"
#include "Node.h"

static std::list<Position> result;

std::list<Position> & Astar (Position const & start, Position const & end, Data const & data)
{
	// Clear the list of previous results
	result.clear();

	// Convenience index for node evaluation
	unsigned int index = 0;

	// Matrix representation of directions (0: East, 1: North, 2: West, 3: South)
	int directions_map[max_x][max_y];

	// Matrix representation of visited and pending nodes
	int visited_nodes[max_x][max_y] = { 0 };
	int pending_nodes[max_x][max_y] = { 0 };

	// Two dimensional priority queue for node evaluation
	std::priority_queue<Node> queue[2];

	// Current position
	Position current_position;
	// Moving directions
	int next_x, next_y;

	// Create a starting node that evaluates the pending distance in a priority queue
	Node node1(start, 0, 0);
	node1.evaluateDistance(end);
	queue[index].push(node1);
	Node node2(start, 0, 0);

	// Algorithm
	while (!queue[index].empty())
	{
		// Get first node from the priority queue (the one with the lowest f value)
		node1 = queue[index].top();
		current_position = node1.getPosition();
		// And remove node from the queue
		queue[index].pop();

		// Mark that is not pending
		pending_nodes[current_position.getX()][current_position.getY()] = 0;
		visited_nodes[current_position.getX()][current_position.getY()] = 1;

		// Check arrival to destination
		if (current_position == end)
		{
			int j;
			result.push_front(current_position);
			while (current_position != start)
			{
				j = directions_map[current_position.getX()][current_position.getY()];
				current_position.set(current_position.getX() + directions_x[j], current_position.getY() + directions_y[j]);
				result.push_front(current_position);
			}
			return result;
		}

		// Inspect all posible directions
		for(unsigned int i = 0; i < available_directions; ++i)
		{
			next_x = current_position.getX() + directions_x[i];
			next_y = current_position.getY() + directions_y[i];

			// Check if it's an empty position not visited yet
			if(!(next_x < 0 || next_x > max_x - 1 || next_y < 0 || next_y > max_y - 1 || data[current_position] == false || visited_nodes[next_x][next_y] == 1))
			{
				// Generate a child node
				node2 = Node( Position(next_x, next_y), node1.getGval(), node1.getFval());
				node2.updateCovered(i);
				node2.evaluateDistance(end);

				// Add to the list of pending nodes if not there
				if (pending_nodes[next_x][next_y] == 0)
				{
					pending_nodes[next_x][next_y] = node2.getFval();
					queue[index].push(node2);
					// Mark its parent node direction
					directions_map[next_x][next_y] = (i + available_directions/2) % available_directions;
				}
				// If already in the list of pending nodes
				else if (pending_nodes[next_x][next_y] > node2.getFval()) 
				{
					// Update the f_val info
					pending_nodes[next_x][next_y] = node2.getFval();

					// Update the parent direction info, mark its parent node direction
					directions_map[next_x][next_y] = (i + available_directions/2) % available_directions;

					// Replace the node by emptying one queue into the other one
					// except the node to be replaced will be ignored
					// and the new node will be pushed in instead
					while (!(queue[index].top().getPosition().getX() == next_x && queue[index].top().getPosition().getY() == next_y))
					{
						queue[1 - index].push(queue[index].top());
						queue[index].pop();
					}

					// Remove the wanted node
					queue[index].pop(); 

					// Empty the larger size queue to the smaller one
					if(queue[index].size() > queue[1 - index].size()) 
					{
						index = 1 - index;
					}
					while(!queue[index].empty())
					{
						queue[1 - index].push(queue[index].top());
						queue[index].pop();
					}
					index = 1 - index;

					// Add the better node instead
					queue[index].push(node2);
				}
			}
		}
	}
	//TODO: No valid route
	return result;
}
