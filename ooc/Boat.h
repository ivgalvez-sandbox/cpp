//////////////////////////////////////////////////////////////////////////////
/// \file Boat.h
///
/// \author Ivan Galvez
/// \date 03/07/2013
/// \ingroup commands
///
/// \brief Boat command interface
///
/// Copyright 2013-2016 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//////////////////////////////////////////////////////////////////////////////

#ifndef BOAT_H
#define BOAT_H

#include "Vehicle.h"

extern const void * Boat;

//! \brief Interface for running the boat
void Run (const void * self);

//! \brief Initialize the boat class and object interface
void initBoat (void);

#endif //BOAT_H
