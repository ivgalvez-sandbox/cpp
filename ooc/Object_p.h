//////////////////////////////////////////////////////////////////////////////
/// \file Object_p.h
///
/// \author Ivan Galvez
/// \date 09/05/2013
/// \ingroup common
///
/// \brief Memory management for abstract data types (Private header)
///
/// \warning This file (*_p.h) contains a private header to be known only by 
/// the implementation of the functionality (*.c). Users should include just 
/// the file *.h, which defines the public interface.
///
/// Based on code from "Object Oriented Programming with ANSI C", 
/// by Axel T. Schreiner (c) 1993.
///
/// Copyright 2013-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//////////////////////////////////////////////////////////////////////////////

//! \addtogroup common Common: Classes and functions of common use.
//! @{
#ifndef OBJECT_P_H
#define OBJECT_P_H

#include <stdarg.h>
#include <stddef.h>

//! Generic object structure
struct Object
{
	//! Object's description
	const struct Class * class;
};


//! \brief Struct to store data related with a generic class.
struct Class
{
	//! Class' description
	const struct Object _;
	//! Class' name
	const char * name;
	//! Class' super class
	const struct Class * super;
	//! Class' object's size
	size_t size;
	//! Class' constructor
	void * (* ctor) (void * _self, va_list * app);
	//! Class' destructor
	void * (* dtor) (void * _self);
};

//! Super class constructor
void * super_ctor (const void * _class, void * _self, va_list * app);

//! Super class destructor
void * super_dtor (const void * _class, void * _self);

#endif //OBJECT_P_H
//! @}
