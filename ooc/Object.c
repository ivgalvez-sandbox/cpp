//////////////////////////////////////////////////////////////////////////////
/// \file Object.c
///
/// \author Ivan Galvez
/// \date 09/05/2013
/// \ingroup common
///
/// \brief Memory management for abstract data types
///
/// Based on code from "Object Oriented Programming with ANSI C", 
/// by Axel T. Schreiner (c) 1993.
///
/// Copyright 2013-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//////////////////////////////////////////////////////////////////////////////

//! \addtogroup common Common: Classes and functions of common use.

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <stdarg.h>

#include "Object.h"
#include "Object_p.h"

// Define stack size
#ifdef STACK
#	if ! defined STACKSIZE || STACKSIZE < 1
#	define STACKSIZE 256
#	endif //STACKSIZE
static int heap[STACKSIZE];
static int * last = heap + 1;
#endif //STACK


static void * Object_ctor (void * _self, va_list * app)
{
	return _self;
}

static void * Object_dtor (void * _self)
{
	return _self;
}

const void * classOf (const void * _self)
{
	const struct Object * self = _self;

	assert(self && self->class);
	return self->class;
}

size_t sizeOf (const void * _self)
{
	const struct Class * class = classOf(_self);

	return class->size;
}

static void * Class_ctor (void * _self, va_list * app)
{
	struct Class * self = _self;
	const size_t offset = offsetof(struct Class, ctor);

	self->name = va_arg(* app, char *);
	self->super = va_arg(* app, struct Class *);
	self->size = va_arg(* app, size_t);

	assert(self->super);

	memcpy((char *)self + offset, (char *)self->super + offset, sizeOf(self->super) - offset);
	{
		typedef void (* voidf) (); // Generic function pointer
		voidf selector;

		// Copying the va_list is a bit tricky on GCC
		va_list ap;
		// 32 bits GCC, C89 or MSVC
		/*
			ap = * app;
		*/
		// 64 bits GCC or C99
		va_copy(ap, * app);

		while ((selector = va_arg(ap, voidf)) != 0)
		{
			voidf method = va_arg(ap, voidf);

			if (selector == (voidf) ctor)
			{
				* (voidf *) & self->ctor = method;
				continue;
			}
			if (selector == (voidf) dtor)
			{
				* (voidf *) & self->dtor = method;
				continue;
			}
		}
		return self;
	}
}

static void * Class_dtor (void * _self)
{
	//struct Class * self = _self;
	return 0;
}

const void * super (const void * _self)
{
	const struct Class * self = _self;

	assert(self && self->super);
	return self->super;
}

void * new (const void * _class, ...)
{
	const struct Class * class = _class;
	va_list ap;

#ifdef STACK
	// Declare a pointer to the stack
	int * object; /* & heap[1..] */
#else
	// Declare the object that will store the heap
	struct Object * object; 
#endif //STACK

	assert(class && class->size);

#ifdef STACK
	// Create the stack storage
	object = last;
	assert(object < heap + STACKSIZE);
	((struct Object * )object)->class = class;
	last = (int *) ( (int)last + class->size );
#else
	// Create the heap storage
	object = calloc(1, class->size);
	assert(object);
	object->class = class;
#endif //STACK

	va_start(ap, _class);
	object = ctor(object, & ap);
	va_end(ap);
	return object;
}

void delete (void * _self)
{
#ifdef STACK
	int * item = _self;

	if (item)
	{
		assert(item > heap && item < heap + STACKSIZE);
		last = (int *) ( (int)last - ((struct Object *)item)->class->size );
		* item = 0;
	}
#else
	if (_self)
	{
		free(dtor(_self));
	}
#endif
}

void * ctor (void * _self, va_list * app)
{
	const struct Class * class = classOf(_self);

	assert(class->ctor);
	return class->ctor(_self, app);
}

void * super_ctor (const void * _class, void * _self, va_list * app)
{
	const struct Class * superclass = super(_class);

	assert(_self && superclass->ctor);
	return superclass->ctor(_self, app);
}

void * dtor (void * _self)
{
	const struct Class * class = classOf(_self);

	assert(class->dtor);
	return class->dtor(_self);
}

void * super_dtor (const void * _class, void * _self)
{
	const struct Class * superclass = super(_class);

	assert(_self && superclass->dtor);
	return superclass->dtor(_self);
}

static const struct Class _Object;
static const struct Class _Class;

static const struct Class _Object =
{
	{ & _Class},
	"Object", & _Object, sizeof(struct Object),
	Object_ctor, Object_dtor
};

static const struct Class _Class =
{
	{ & _Class },
	"Class", & _Object, sizeof(struct Class),
	Class_ctor, Class_dtor
};

const void * Object = & _Object;
const void * Class = & _Class;
