/////////////////////////////////////////////////////////////////////////////
/// \file Boat.c
///
/// \author Ivan Galvez
/// \date 03/07/2013
///
/// \brief Implementation of Boat object
///
/// Copyright 2013-2016 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

#include "Boat.h"
#include "Boat_p.h"

static void * Boat_ctor (void * _self, va_list * app)
{
	struct Boat * self = super_ctor(Boat, _self, app);
	return self;
}

static void Boat_Run (const void * _self)
{
   printf ("Running a Boat\n");
}

const void * Boat;

void initBoat (void)
{
	if (!Boat)
	{
		initVehicle();
		Boat = new(VehicleClass, "Boat", Vehicle, sizeof(struct Boat), ctor, Boat_ctor, Run, Boat_Run, 0);
	}
}
