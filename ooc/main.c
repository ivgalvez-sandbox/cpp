///////////////////////////////////////////////////////////////////////////////
/// \file main.c
///
/// \author Ivan Galvez Junquera
/// \date 04/06/2014
///
/// \brief Example of use of object oriented C.
///
/// Copyright 2014-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>

#include "Car.h"
#include "Boat.h"

void * pVehicle;

int main (int argc, char *argv[])
{
	initCar();
	initBoat();
	
	if (!strcmp(argv[1], "boat"))
	{
		printf ("Create a new Vehicle object of type Boat\n");
		pVehicle = new(Boat, 2);
	}
	else if (!strcmp(argv[1], "car"))
	{
		printf ("Create a new Vehicle object of type Car\n");
		pVehicle = new(Car, 4);
	}
	else
	{
		printf ("No known vehicles\n");
		return 1;
	}

	Run(pVehicle);
	printf ("With %d passengers\n", getPassengers(pVehicle));
}
