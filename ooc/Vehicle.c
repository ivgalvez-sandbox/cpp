/////////////////////////////////////////////////////////////////////////////
/// \file Vehicle.c
///
/// \author Ivan Galvez
/// \date 24/06/2013
///
/// \brief Implementation of parent object for vehicles
///
/// Copyright 2013-2016 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//////////////////////////////////////////////////////////////////////////////

#include <assert.h>
#include <stdio.h>

//Object interface
#include "Object.h"
#include "Object_p.h"

#include "Vehicle.h"
#include "Vehicle_p.h"

static void * Vehicle_ctor (void * _self, va_list * app)
{
	struct Vehicle * self = _self;
	self->passengers = (uint8_t) va_arg(* app, int);
	return self;
}

uint8_t getPassengers (void * _self)
{
	struct Vehicle * self = _self;
	return self->passengers;
}

void Run (const void * _self)
{
	const struct VehicleClass * class = classOf(_self);
	assert(class -> Run);
	class -> Run(_self);
}

void super_Run (const void * _class, const void * _self)
{
	const struct VehicleClass * superclass = super(_class);
	assert(_self && superclass -> Run);
	superclass -> Run(_self);
}

static void * VehicleClass_ctor (void * _self, va_list * app)
{
	struct VehicleClass * self = super_ctor(VehicleClass, _self, app);
	typedef void (* voidf) ();
	voidf selector;

	// Copying the va_list is a bit tricky on GCC
	va_list ap;
	// 32 bits GCC, C89 or MSVC
	/*
		ap = * app;
	*/
	// 64 bits GCC or C99
	va_copy(ap, * app);

	while ((selector = va_arg(ap, voidf)) != 0)
	{
		voidf method = va_arg(ap, voidf);

		if (selector == (voidf) Run)
		{
			* (voidf *) & self -> Run = method;
		}
	}
	return self;
}

const void * VehicleClass, * Vehicle;

void initVehicle (void)
{
	if (!VehicleClass)
	{
		VehicleClass = new(Class, "VehicleClass", Class, sizeof(struct VehicleClass), ctor, VehicleClass_ctor, 0);
	}
	if (!Vehicle)
	{
		Vehicle = new(VehicleClass, "Vehicle", Object, sizeof(struct Vehicle), ctor, Vehicle_ctor, Run, 0);
	}
}
