//////////////////////////////////////////////////////////////////////////////
/// \file Object.h
///
/// \author Ivan Galvez
/// \date 09/05/2013
/// \ingroup common
///
/// \brief Memory management for abstract data types
///
/// Based on code from "Object Oriented Programming with ANSI C", 
/// by Axel T. Schreiner (c) 1993.
///
/// Copyright 2013-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//////////////////////////////////////////////////////////////////////////////

//! \addtogroup common Common: Classes and functions of common use.
//! @{

#ifndef OBJECT_H
#define OBJECT_H

#include <stdarg.h> // For va_list
#include <stdio.h> // For size_t

//! \brief Pointer to generic object.
extern const void * Object;

//! \brief Create a new object of type '_class'
//! The new created object will reside on heap or stack memory depending on 
//! the specific implementation of this function
//!
//! \param _class Object type to be created
//! \param ... Include any additional parameters needed by the object to be created.
//! \returns A generic void * with the address of the created object.
void * new (const void * _class, ...);

//! \brief Deletes the provided object
//! \param _self Pointer to object to be deleted.
void delete (void * _self);

const void * classOf (const void * _self);
size_t sizeOf (const void * _self);

void * ctor (void * _self, va_list * app);
void * dtor (void * _self);

extern const void * Class;
const void * super (const void * _self);

#endif //OBJECT_H
//! @}
