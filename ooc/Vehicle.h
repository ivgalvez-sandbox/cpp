//////////////////////////////////////////////////////////////////////////////
/// \file Vehicle.h
///
/// \author Ivan Galvez
/// \date 24/06/2013
///
/// \brief Vehicles interface
///
/// Copyright 2013-2016 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//////////////////////////////////////////////////////////////////////////////

#ifndef VEHICLE_H
#define VEHICLE_H

#include <stdint.h>

//Object interface
#include "Object.h"

//! \brief Pointer to object of type Vehicle.
//! \sa
//! It must be created using the ::new idiom provided in module ::common:
//!
//! <CODE>
//! pVehicle = new(Vehicle, uint8_t passengers);
//! </CODE>
//!
//! \param passengers Specifies the number of passengers.
//! \returns A void * with the address to the object Vehicle.
//!
//! \warning
//! If the memory manager implemented by ::new creates the object in the heap,
//! the memory must be released manually, using ::delete, to avoid any leak.
//! Best practice is to always release the object:
//!
//! <CODE>
//! delete(pVehicle);
//! </CODE>
extern const void * Vehicle;
extern const void * VehicleClass;

//! \brief Initialize the vehicle class and object interface
void initVehicle (void);

//! \brief Interface for running the vehicle
//! 
//! \param self Pointer to the Vehicle object.
void Run (const void * self);
uint8_t getPassengers (void * self);

#endif //VEHICLE_H
