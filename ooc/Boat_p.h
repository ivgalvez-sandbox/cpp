//////////////////////////////////////////////////////////////////////////////
/// \file Boat_p.h
///
/// \author Ivan Galvez
/// \date 03/07/2013
///
/// \brief Internal structure Boat objects (Private header).
///
/// \warning This file (*_p.h) contains a private header to be known only by 
/// the implementation of the functionality (*.c). Users should include just 
/// the file *.h, which defines the public interface.
///
/// Copyright 2013-2016 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//////////////////////////////////////////////////////////////////////////////

#ifndef BOAT_P_H
#define BOAT_P_H

#include <stdint.h>
#include "Vehicle_p.h"

//! \brief Struct to store data related with Boat
struct Boat
{
	const struct Vehicle _;
};

#endif //BOAT_P_H
