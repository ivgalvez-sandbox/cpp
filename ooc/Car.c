/////////////////////////////////////////////////////////////////////////////
/// \file Car.c
///
/// \author Ivan Galvez
/// \date 03/07/2013
///
/// \brief Implementation of Car object
///
/// Copyright 2013-2016 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

#include "Car.h"
#include "Car_p.h"

static void * Car_ctor (void * _self, va_list * app)
{
	struct Car * self = super_ctor(Car, _self, app);
	return self;
}

static void Car_Run (const void * _self)
{
	printf ("Running a Car\n");
}

const void * Car;

void initCar (void)
{
	if (!Car)
	{
		initVehicle();
		Car = new(VehicleClass, "Car", Vehicle, sizeof(struct Car), ctor, Car_ctor, Run, Car_Run, 0);
	}
}
