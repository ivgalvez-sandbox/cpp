//////////////////////////////////////////////////////////////////////////////
/// \file Node.h
///
/// \author Ivan Galvez Junquera
/// \date 30/06/2016
///
/// \brief Multinode tree data structure
///
/// Copyright 2016 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

#ifndef NODE_H
#define NODE_H

#include <set>
#include <memory>
#include <functional>
#include <cstdint>

// Forward declare for template friends.
template <typename T> class Node;
template <typename T> std::ostream & operator << (std::ostream & ostr, Node<T> const & val);
template <typename T> bool operator < (Node<T> const & lhs, Node<T> const & rhs );
template <typename T> bool operator == (Node<T> const & lhs, Node<T> const & rhs );
template <typename T> bool operator != (Node<T> const & lhs, Node<T> const & rhs );

template <typename T>
class Node : public std::enable_shared_from_this<Node<T>>
{
	public:
		typedef std::shared_ptr<Node> ptr;

		//! \brief Operator insertion on a stream.
		//!
		//! \param ostr The destination output stream.
		//! \param val Instance to display.
		friend std::ostream & operator << <>(std::ostream & ostr, Node<T> const & val);

		//! \brief Comparison operator, minor than.
		//!
		//! \param lhs A Node object to be compared.
		//! \param rhs A Node object to compared with.
		friend bool operator < <> (Node<T> const & lhs, Node<T> const & rhs );

		//! \brief Comparison operator, equal to.
		//!
		//! \param lhs A Node object to be compared.
		//! \param rhs A Node object to compared with.
		friend bool operator == <> (Node<T> const & lhs, Node<T> const & rhs );

		//! \brief Comparison operator, not equal to.
		//!
		//! \param lhs A Node object to be compared.
		//! \param rhs A Node object to compared with.
		friend bool operator != <> (Node<T> const & lhs, Node<T> const & rhs );

		struct less_ : std::binary_function<Node<T>::ptr, Node<T>::ptr, bool> 
		{
			bool operator() (Node<T>::ptr const & b1, Node<T>::ptr const & b2 )
			{
				return ( b1->data < b2->data );
			}
		};
	

	public:
		//! \brief Constructor receiving data
		Node(T var);

		//! \brief Use provided default constructor
		Node() = default;
		
		//! \brief Add a new child to the existing node
		void addChild (ptr node);
		void addChild (Node && node);

		//! \brief Reference to parent
		ptr getParent();

		//! \brief Get node level on the tree
		uint16_t getLevel();

		//! \brief Set node level on the tree
		void setLevel(uint16_t level);

		//! \brief Data contained in the node
		T data;
	
		//! \brief Set of childs
		std::set<ptr, less_> childs;

	
	private:
		//! \brief Display the object in an output stream.
		//!
		//! \param ostr The destination output stream.
		void display(std::ostream & ostr) const;

	private:
		ptr parent;

		uint16_t m_level = 0;

};

template <typename T>
Node<T>::Node(T var) 
	: data{var}
{
	std::cout << "Create Node: " << data << std::endl;
};

template <typename T>
void Node<T>::addChild (Node<T>::ptr node)
{
   node->setLevel(m_level + 1);
	node->parent = this->shared_from_this();
	childs.insert(node);
	std::cout << "- Add " << node->data << " to " << "[" << node->getLevel() << "] "<< data << std::endl;
}

template <typename T>
void Node<T>::addChild (Node<T> && node)
{
   node.setLevel(m_level + 1);
	node.parent = this->shared_from_this();
	childs.insert(std::make_shared<Node<T>>(node));
	std::cout << "- Add " << node.data << " to " << "[" << node.getLevel() << "] "<< data << std::endl;
}

template <typename T>
typename Node<T>::ptr Node<T>::getParent()
{
	return parent;
}

template <typename T>
uint16_t Node<T>::getLevel()
{
	return m_level;
}

template <typename T>
void Node<T>::setLevel(uint16_t level)
{
   m_level = level;
}

template <typename T>
void Node<T>::display(std::ostream & ostr) const
{
   ostr << data;
}

template <typename T>
std::ostream & operator << (std::ostream & ostr, Node<T> const & val)
{
	val.display(ostr);
	return ostr;
}

template <typename T>
bool operator < (Node<T> const & lhs, Node<T> const & rhs )
{
	return lhs.data < rhs.data;
}

template <typename T>
bool operator == (Node<T> const & lhs, Node<T> const & rhs )
{
	return lhs.data == rhs.data;
}

template <typename T>
bool operator != (Node<T> const & lhs, Node<T> const & rhs )
{
	return lhs.data != rhs.data;
}

#endif							 //NODE_H
