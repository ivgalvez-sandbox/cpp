///////////////////////////////////////////////////////////////////////////////
/// \file StateMachine.h
///
/// \author Ivan Galvez Junquera
/// \date 04/06/2014
///
/// \brief A simple state machine in ANSI C using a function table.
///
/// Copyright 2014-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///
///////////////////////////////////////////////////////////////////////////////

#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

// Possible states
typedef enum
{
	STATE_INIT = 0,
	STATE_PAINT,
	STATE_CLICK,
	STATE_TERMINATE,
	NUM_STATES,
} State_t;

//Events
typedef enum
{
	EVENT_MOUSEMOVE,
	EVENT_KEYPRESS,
} Event_t;

typedef State_t State_func_t(Event_t event);

// State functions
State_t do_state_init(Event_t event);
State_t do_state_paint(Event_t event);
State_t do_state_click(Event_t event);
State_t do_state_terminate(Event_t event);

// State table
extern State_func_t * const state_table[NUM_STATES];

// State runner
State_t run_state(State_t current_state, Event_t event);

#endif //STATE_MACHINE_H
