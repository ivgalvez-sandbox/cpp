///////////////////////////////////////////////////////////////////////////////
/// \file main.c
///
/// \author Ivan Galvez Junquera
/// \date 04/06/2014
///
/// \brief A simple state machine in ANSI C using a function table. 
/// Example of use.
///
/// Copyright 2014-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

#include "StateMachine.h"

int main( void )
{
	char buffer[256];
	State_t state = STATE_INIT;
	Event_t event;

	while (state != STATE_TERMINATE)
	{
		printf ("State: ");
		if (state == STATE_INIT)
		{
			printf ("STATE_INIT\n");
		}
		if (state == STATE_PAINT)
		{
			printf ("STATE_PAINT\n");
		}
		if (state == STATE_CLICK)
		{
			printf ("STATE_CLICK\n");
		}
		if (state == STATE_TERMINATE)
		{
			printf ("STATE_TERMINATE\n");
		}

		printf ("Enter action: ");
		fgets (buffer, 256, stdin);
		event = (Event_t)(atoi (buffer));

		printf ("Event: ");
		if (event == EVENT_KEYPRESS)
			printf ("EVENT_KEYPRESS\n");
		if (event == EVENT_MOUSEMOVE)
			printf ("EVENT_MOUSEMOVE\n");

		state = run_state(state, event);
	}
}
