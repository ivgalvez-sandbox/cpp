///////////////////////////////////////////////////////////////////////////////
/// \file StateMachine.c
///
/// \author Ivan Galvez Junquera
/// \date 04/06/2014
///
/// \brief A simple state machine in ANSI C using a function table.
///
/// Copyright 2014-2015 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

#include "StateMachine.h"

State_func_t * const state_table[NUM_STATES] =
{
	do_state_init,
	do_state_paint,
	do_state_click,
	do_state_terminate,
};

State_t run_state(State_t current_state, Event_t event)
{
	return state_table[current_state](event);
};

State_t do_state_init(Event_t event)
{
	printf ("[do_state_init]\n");
	if (event == EVENT_MOUSEMOVE)
		return STATE_PAINT;
	
	return STATE_INIT;
}

State_t do_state_paint(Event_t event)
{
	printf ("[do_state_paint]\n");
	if (event == EVENT_KEYPRESS)
		return STATE_CLICK;

	return STATE_PAINT;;
}

State_t do_state_click(Event_t event)
{
	printf ("[do_state_click]\n");
	if (event == EVENT_KEYPRESS)
		return STATE_TERMINATE;

	return STATE_CLICK;
}

State_t do_state_terminate(Event_t event)
{
	printf ("[do_state_terminate]\n");

	return STATE_TERMINATE;
}
