///////////////////////////////////////////////////////////////////////////////
/// \file AssertionFailure.h
///
/// \author Ivan Galvez Junquera
/// \date 15/12/2006
///
/// \brief Implementation of exception AssertionFAilure 
///
/// Copyright 2006-2016 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

//! \addtogroup common Common: Classes and functions of common use.
//! @{

#ifndef ASSERTION_FAILURE_H
#define ASSERTION_FAILURE_H

#include "Exception.h"

namespace common
{
	//! \brief Exception raised in case of assertion fail.
	//!
	//! Together with the macro ASSERT allows to replace the use of C++ macro 'assert' 
	//! (which is only compiled in DEBUG mode) to avoid continuing the execution also
	//! in a RELEASE version.
	//!
	//! \sa ASSERT
	//! \ingroup common
	//! \author Ivan Galvez Junquera
	class AssertionFailure : public Exception
	{
		public:
			//! \brief Constructor.
			//!
			//! \param text A simple string describing the condition of the exception.
			//! \param where An optional string describing the location in
			//!          the code from where the exception was raised.
			inline AssertionFailure(const std::string & text, const std::string & where = "")
				: Exception(std::string("AssertionFailure - ").append(text), where)
			{}
	};
}
#endif //ASSERTION_FAILURE_H
//! @}
