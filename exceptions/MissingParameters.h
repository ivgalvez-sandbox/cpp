///////////////////////////////////////////////////////////////////////////////
/// \file MissingParameters.h
///
/// \author Ivan Galvez
/// \date 10/03/2014
///
/// \brief Implementation of exception MissingParameters
///
/// Copyright 2014-2016 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

//! \addtogroup common Common: Classes and functions of common use.
//! @{

#ifndef MISSING_PARAMETERS_H
#define MISSING_PARAMETERS_H

#include "Exception.h"

namespace common
{
	//! \brief Exception raised in case of missing parameters.
	//!
	//! \ingroup common
	//! \author Ivan Galvez
	class MissingParameters : public Exception
	{
		public:
			//! \brief Constructor.
			//!
			//! \param text A simple string describing the condition of the exception.
			//! \param where An optional string describing the location in
			//!        the code from where the exception was raised.
			inline MissingParameters(const std::string & text, const std::string & where = "")
				: Exception(std::string("MissingParameters - ").append(text), where)
			{}
	};
}
#endif //MISSING_PARAMETERS_H
//! @}
