///////////////////////////////////////////////////////////////////////////////
/// \file BadConversion.h
///
/// \author Ivan Galvez Junquera
/// \date 15/12/2006
///
/// \brief Implementation of exception BadConversion 
///
/// Copyright 2006-2016 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

//! \addtogroup common Common: Classes and functions of common use.
//! @{

#ifndef BAD_CONVERSION_H
#define BAD_CONVERSION_H

#include "Exception.h"

namespace common
{
	//! \brief Exception raised in case of type conversion error.
	//!
	//! \ingroup common
	//! \author Ivan Galvez Junquera
	class BadConversion : public Exception
	{
		public:
			//! \brief Constructor.
			//!
			//! \param text A simple string describing the condition of the exception.
			//! \param where An optional string describing the location in
			//!        the code from where the exception was raised.
			inline BadConversion(const std::string & text, const std::string & where = "")
				: Exception(std::string("BadConversion - ").append(text), where)
			{}
	};
}
#endif //BAD_CONVERSION_H
//! @}
