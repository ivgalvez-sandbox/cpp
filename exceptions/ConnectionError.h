///////////////////////////////////////////////////////////////////////////////
/// \file ConnectionError.h
///
/// \author Ivan Galvez Junquera
/// \date 15/12/2006
///
/// \brief Implementation of exception ConnectionError 
///
/// Copyright 2006-2016 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

//! \addtogroup common Common: Classes and functions of common use.
//! @{

#ifndef CONNECTION_ERROR_H
#define CONNECTION_ERROR_H

#include "Exception.h"

namespace common
{
	//! \brief Exception raised in case of connection error.
	//!
	//! \ingroup common
	//! \author Ivan Galvez Junquera
	class ConnectionError : public Exception
	{
		public:
			//! \brief Constructor.
			//!
			//! \param text A simple string describing the condition of the exception.
			//! \param where An optional string describing the location in
			//!        the code from where the exception was raised.
			inline ConnectionError(const std::string & text, const std::string & where = "")
				: Exception(std::string("ConnectionError - ").append(text), where)
			{}
	};
}
#endif //CONNECTION_ERROR_H
//! @}
