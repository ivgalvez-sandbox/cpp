///////////////////////////////////////////////////////////////////////////////
/// \file Exceptions.h
///
/// \author Ivan Galvez Junquera
/// \date 15/12/2006
///
/// \brief Common header for exceptions management.
///
/// Copyright 2006-2016 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

//! \addtogroup common Common: Classes and functions of common use.
//! @{

#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include "Exception.h"
#include "ExceptionMacros.h"
#include "AssertionFailure.h"
#include "ConnectionError.h"
#include "BadConversion.h"
#include "FileError.h"
#include "MissingParameters.h"
#include "RuntimeError.h"

#endif //EXCEPTIONS_H
//! @}
