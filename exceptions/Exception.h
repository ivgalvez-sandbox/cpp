///////////////////////////////////////////////////////////////////////////////
/// \file Exception.h
///
/// \author Ivan Galvez Junquera
/// \date 15/12/2006
///
/// \brief Base classes for definition and management of exceptions.
///
/// Copyright 2006-2016 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

//! \addtogroup common Common: Classes and functions of common use.
//! @{

#ifndef EXCEPTION_H
#define EXCEPTION_H

//System headers
#include <string>
#include <stdexcept>

namespace common
{
	//! \brief Simple base class for definition and management of exceptions.
	//!
	//! Defines a common interface for all the exceptions.
	//! Inherits from std::exception so it can be captured as a generic exception.
	//!
	//! \sa THROW
	//! \ingroup common
	//! \author Ivan Galvez Junquera
	class Exception: virtual public std::exception
	{
		public:
			//! \brief Default constructor.
			//!
			//! \param text A simple string describing the condition of the exception.
			//! \param where An optional string describing the location in
			//!   		 the code from where the exception was raised.
			inline Exception(const std::string & text, const std::string & where = "")
				: m_description(text)
			{
				m_description.append(" " + where);
			}

			//! \brief Destructor.
			//!
			//! \exception Defined only to keep compatibility with the std::exception interface.
			inline virtual ~Exception() throw()
			{}

			//! \brief Gets the description of the exception condition.
			//!
			//! \returns A char * with the description of the exception.
			//!
			//! \exception Defined only to keep compatibility with the std::exception interface.
			inline const char * what() const throw()
			{
				return m_description.c_str();
			}

			//! \brief Gets the description of the exception condition.
			//!
			//! \returns A std::string with the description of the exception.
			inline const std::string & what_as_string() const
			{
				return m_description;
			}

			//! \brief Adds a string to the description of the exception.
			//!
			//! \param text char * that is added to the description of the exception.
			//!
			//! \warning A blank space is inserted before the text to avoid overlapping the original text.
			//!
			//! \returns A reference to the modified exception.
			inline Exception & append(const char * text)
			{
				m_description.append(" " + std::string(text));
				return * this;
			}

			//! \brief Adds a string to the description of the exception.
			//!
			//! \param text std::string that is added to the description of the exception.
			//!
			//! \warning A blank space is inserted before the text to avoid overlaqpping the original text.
			//!
			//! \returns A reference to the modified exception.
			inline Exception & append(const std::string & text)
			{
				m_description.append(" " + text);
				return * this;
			}

		protected:
			//! Stores the description of the exception.
			std::string m_description; 
	};
}
#endif //EXCEPTION_H
//! @}
