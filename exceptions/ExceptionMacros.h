///////////////////////////////////////////////////////////////////////////////
/// \file ExceptionMacros.h
///
/// \author Ivan Galvez Junquera
/// \date 15/12/2006
///
/// \brief Some useful macros for exception management.
///
/// Copyright 2006-2016 Ivan Galvez Junquera (ivgalvez@gmail.com).
/// http://gitlab.com/ivgalvez-sandbox
///
/// Distributed under the OSI-approved BSD "July 22 1999" style license;
/// Complete text of the original BSD "July 22 1999" license can be found in
/// /usr/share/common-licenses/BSD on Debian systems.
///
/// This software is distributed WITHOUT ANY WARRANTY; without even the
/// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
///////////////////////////////////////////////////////////////////////////////

//! \addtogroup common Common: Classes and functions of common use.
//! @{

#ifndef EXCEPTION_MACROS_H
#define EXCEPTION_MACROS_H

#include "Exception.h"

// Macros used for exception raising with debugging information.
#if (NDEBUG == 1)
#	define THROW(exType, report) \
	throw exType(report)
#else
#	define __STR(x) __VAL(x)
#	define __VAL(x) #x
//! \def THROW
//!
//! \brief Can replace the use of regular 'throw' idiom to add more debugging infomation.
//!
//! When compiling in DEBUG, completes automatically the parameter 'where'
//! including the file name and line number from which the exception is raised.
//! When compiling in RELEASE, the exception is raised in the conventional way, without
//! including any additional information.
//!
//! \param exType Exception type.
//! \param report A string describing the condition of exception.
//! 
//! \ingroup common
//! \author Ivan Galvez Junquera
#	define THROW(exType, report) \
	throw exType( report, " [in " __FILE__ " line: " __STR(__LINE__) "]" )
#endif // (NDEBUG == 1)

//! \def ASSERT
//!
//! \brief Can replace the use of regular 'assert' idiom to add more debugging infomation..
//!
//! Using this macro has the same effect of assert, but the code generated
//! compiles in both DEBUG and RELEASE versions. Also an exception of type
//! AssertionFailure is raised including the condition test that failed and, in 
//! DEBUG version, the line of the code in which the error happened.
//!
//! \param test Condition to be evaluated.
//!
//! \par Example:
//! <c> ASSERT (a==3); </c>
//!
//! \ingroup common
//! \author Ivan Galvez Junquera
#define ASSERT(test) \
if (!(test)) THROW( AssertionFailure, #test );

#endif //EXCEPTION_MACROS_H
//! @}
